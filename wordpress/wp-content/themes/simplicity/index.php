<?php get_header(); ?>
<?php sy_pre_content(); ?>
<div id="content">

  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <div class="post" id="post-<?php the_ID(); ?>">
    <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>

    <div class="entry">
    <?php the_content('Read the rest of this entry &raquo;'); ?>
    </div>

    <p class="postmetadata">
<?php if ($sy->option['show_tags_in_home']) {
          the_tags('Tags: ', ', ', '<br />'); 
      }
?> Posted 
<?php the_time('F jS, Y') ?> in <?php the_category(', ') ?> 
<?php if ($sy->option['show_author_in_home']) { ?> By
<?php the_author_posts_link(); ?>
<?php } ?>
|
<?php edit_post_link('Edit', '', ' | '); ?>
<?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
    </p>
    </div>

    <?php endwhile; ?>

<div class="navigation">
<div class="alignleft"><?php next_posts_link('&laquo; Older Entries &laquo;') ?></div>
<div class="alignright"><?php previous_posts_link('&raquo; Newer Entries &raquo;') ?></div>
</div>

  <?php else : ?>

    <h2 class="center">Not Found</h2>
    <p class="center">Sorry, but you are looking for something that isn't here.</p>
    <?php include (TEMPLATEPATH . "/searchform.php"); ?>

  <?php endif; ?>
</div><!-- content -->
<?php sy_post_content(); ?>
<?php get_footer(); ?>
