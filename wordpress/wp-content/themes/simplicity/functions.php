<?php
include(dirname(__FILE__).'/themetoolkit.php');
if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name'=>'Left Sidebar',
        'before_widget' => '<li>',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name'=>'Right Sidebar',
        'before_widget' => '<li>',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
}
?>
<?php
add_filter('comments_template', 'legacy_comments');
function legacy_comments($file) {
    if(!function_exists('wp_list_comments'))
        $file = TEMPLATEPATH . '/legacy-comments.php';
    return $file;
}
?>
<?php function sy_pre_content() { ?>
<div id="outer-column-container">
<div id="inner-column-container">
<div id="source-order-container">
<div id="content-column">
<?php } ?>
<?php function sy_post_content() {
  global $sy;
?>
</div><!-- content-column -->
</div><!-- source-order-container -->
<?php if ($sy->option['left_sidebar'] == 'enable') { ?>
<div id="left-column">
<?php get_sidebar(); ?>
</div><!-- left-column -->
<?php } ?>
<?php if ($sy->option['right_sidebar'] == 'enable') { ?>
<div id="right-column">
<?php get_sidebar('right'); ?>
</div><!-- right-column -->
<?php } ?>
</div><!-- inner-column-container -->
</div><!-- outer-column-container -->
<?php } ?>
<?php
themetoolkit(
    'sy',
    array(
        'width_type' => 'Fixed or Fluid Page Width {radio|fixed|Fixed|fluid|Fluid}',
        'left_sidebar' => 'Left Sidebar {radio|enable|Enable|disable|Disable}',
        'right_sidebar' => 'Right Sidebar {radio|enable|Enable|disable|Disable}',
        'page_width' => 'Page width ## Page width (for fixed width layout)',
        'page_min_width' => 'Minimum page width ## Minimum age width in pixels (for fluid width layout)',
        'left_sidebar_width' => 'Left sidebar width ## Width of left sidebar in pixels (only if left sidebar is enabled)',
        'right_sidebar_width' => 'Right sidebar width ## Width of right sidebar in pixels (only if right sidebar is enabled)',
        'show_search_in_header' => 'Show Search Box in Header {radio|0|No|1|Yes}',
        'show_tags_in_home' => 'Show tags with post snippet in homepage {radio|0|No|1|Yes}',
        'show_author_in_home' => 'Show author with post snippet in homepage {radio|0|No|1|Yes} ## If your blog has only one author, you probably don\'t want to show the author name since they will all be identical anyway',
        'header_logo_url' => 'Header Logo URL ## URL of the logo image to display in header; Leave blank to disable logo display',
        'header_logo_posn' => 'Header logo position {radio|left|Left|center|Center|right|Right}',
        'header_logo_width' => 'Logo width ## Width of logo in pixels',
        'header_logo_height' => 'Logo height ## Width of logo in pixels',
        'meta_header_description' => 'META Header Description {textarea|4|60} ## Description to appear in the homepage only',
        'meta_header_keywords' => 'META Header Keywords {textarea|4|60} ## Keywords to appear in the homepage only',
    ),
    __FILE__
);

$set_defaults['right_sidebar'] = 'enable';
$set_defaults['left_sidebar'] = 'enable';
$set_defaults['show_search_in_header'] = 1;
$set_defaults['show_tags_in_home'] = 1;
$set_defaults['show_author_in_home'] = 0;
$set_defaults['width_type'] = 'fluid';
$set_defaults['page_width'] = 800;
$set_defaults['page_min_width'] = 320;
$set_defaults['left_sidebar_width'] = 160;
$set_defaults['right_sidebar_width'] = 180;
$set_defaults['header_logo_posn'] = 'left';
$set_defaults['header_logo_url'] = '';
$set_defaults['header_logo_width'] = 60;
$set_defaults['header_logo_height'] = 60;

if (!$sy->is_installed()) {
    $result = $sy->store_options($set_defaults);
}
else {
    foreach (array_keys($set_defaults) as $value) {
        if (!isset($sy->option[$value])) {
            $sy->option[$value] = $set_defaults[$value];
        }
    }
}
?>
