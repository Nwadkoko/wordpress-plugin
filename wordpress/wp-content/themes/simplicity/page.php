<?php get_header(); ?>
<?php sy_pre_content(); ?>
<div id="content">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="post" id="post-<?php the_ID(); ?>">
<h2><?php the_title(); ?></h2>
	<div class="entry">
	<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

	<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

	</div>
</div>
<?php endwhile; endif; ?>
                                                <?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
                                                        // Both Comments and Pings are open ?>
                                                        You can <a href="#respond">leave a response</a>, or <a href="<?php trackback_url(); ?>" rel="trackback">trackback</a> from your own site.

                                                <?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
                                                        // Only Pings are Open ?>
                                                        Responses are currently closed, but you can <a href="<?php trackback_url(); ?> " rel="trackback">trackback</a> from your own site.

                                                <?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
                                                        // Comments are open, Pings are not ?>
                                                        You can skip to the end and leave a response. Pinging is currently not allowed.

                                                <?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
                                                        // Neither Comments, nor Pings are open ?>
                                                        Both comments and pings are currently closed.

                                                <?php } edit_post_link('Edit this entry','','.'); ?>

        <?php comments_template(); ?>

</div><!-- content -->
<?php sy_post_content(); ?>
<?php get_footer(); ?>
