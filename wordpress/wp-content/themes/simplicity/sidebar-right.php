<div class="sidebar">
<ul>
<?php 	/* Widgetized sidebar, if you have the plugin installed. */
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(2) ) : ?>
<li><h3 class="widgettitle">Meta</h3>
<ul>
<?php wp_register(); ?>
<li><?php wp_loginout(); ?></li>
<?php wp_meta(); ?>
</ul>
</li>
<?php if ( function_exists('wp_tag_cloud') ) { ?>
<li><h3 class="widgettitle">Tags</h3>
<?php wp_tag_cloud('format=list'); } ?>
</li>
<li><h3 class="widgettitle">Categories</h3>
<ul>
<?php wp_list_categories('title_li='); ?>
</ul>
</li>
<?php endif; ?>
</ul>
</div>
