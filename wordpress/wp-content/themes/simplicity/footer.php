
<div id="footer">
<p>
<?php bloginfo('name'); ?> is proudly powered by
<a href="http://wordpress.org/">WordPress</a>
and the <a href="http://zitseng.com/tech/simplicity">Simplicity</a> theme.
<br /><a href="<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a>
and <a href="<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a>.
</p>
</div><!-- footer -->
</div><!-- page -->
<?php wp_footer(); ?>
</body>
</html>
