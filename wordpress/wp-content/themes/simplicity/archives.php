<?php get_header(); ?>
<?php sy_pre_content(); ?>
<div id="content">

<?php include (TEMPLATEPATH . '/searchform.php'); ?>

<h2>Archives by Month:</h2>
	<ul>
		<?php wp_get_archives('type=monthly'); ?>
	</ul>

<h2>Archives by Subject:</h2>
	<ul>
		 <?php wp_list_categories(); ?>
	</ul>

</div><!-- content -->
<?php sy_post_content(); ?>
<?php get_footer(); ?>
