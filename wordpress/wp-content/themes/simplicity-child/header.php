<?php
global $sy;
global $options;
/* foreach ($options as $value) {
if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); } }
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php bloginfo('name'); ?><?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?><?php wp_title(); ?></title>
<?php
if ( is_home() && rtrim($sy->option['meta_header_description']) != "" ) { 
  echo "<meta name=\"description\" content=\"{$sy->option['meta_header_description']}\" />\n"; }
if ( is_home() && rtrim($sy->option['meta_header_keywords']) != "" ) { 
  echo "<meta name=\"keywords\" content=\"{$sy->option['meta_header_keywords']}\" />\n"; }
?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/style.css"; ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<style type="text/css" media="screen">
body { }
<?php if ($sy->option['width_type'] == 'fixed') { ?>
#page { width: <?php echo $sy->option['page_width']; ?>px; margin-left: auto; margin-right: auto; }
<?php } elseif ($sy->option['page_min_width']) { ?>
#page { min-width: <?php echo $sy->option['page_min_width']; ?>; }
<?php } ?>
<?php if ($sy->option['left_sidebar'] == 'disable') { ?>
#outer-column-container { border-left: none; }
<?php } elseif ($sy->option['left_sidebar_width']) { ?>
#left-column { margin-left: -100%; width: <?php echo $sy->option['left_sidebar_width']-8 ?>px; }
#content { margin-left: <?php echo $sy->option['left_sidebar_width'] ?>px;  }
#outer-column-container { }
<?php }
if ($sy->option['right_sidebar'] == 'disable') { ?>
#outer-column-container { }
<?php } elseif ($sy->option['right_sidebar_width']) { ?>
#content { margin-right: <?php echo $sy->option['right_sidebar_width'] ?>px; }
#right-column { margin-left: -<?php echo $sy->option['right_sidebar_width'] ?>px; width: <?php echo $sy->option['right_sidebar_width']-8 ?>px; }
#outer-column-container { }
<?php } ?>
</style>

<?php
if (is_singular()) wp_enqueue_script('comment-reply');
wp_head(); ?>
</head>
<body>
<div id="page">


<div id="header">
	<div id="headertitle">
<?php if (rtrim($sy->option['header_logo_url']) != '') { ?>
<?php if ($sy->option['header_logo_posn'] == 'right') { ?>
<img src="<?php echo $sy->option['header_logo_url'] ?>" alt="" style="float: right; margin: 5px 5px 5px 0;
height: <?php echo $sy->option['header_logo_height'] ?>px; width: <?php echo $sy->option['header_logo_width'] ?>px;" />
<?php } elseif ($sy->option['header_logo_posn'] == 'center') { ?>
<img src="<?php echo $sy->option['header_logo_url'] ?>" alt="" style="margin: 0 auto; padding: 5px 0; display: block;
height: <?php echo $sy->option['header_logo_height'] ?>px; width: <?php echo $sy->option['header_logo_width'] ?>px;" />
<?php } else { ?>
<img src="<?php echo $sy->option['header_logo_url'] ?>" alt="" style="float: left; margin: 5px 5px 5px 0;
height: <?php echo $sy->option['header_logo_height'] ?>px; width: <?php echo $sy->option['header_logo_width'] ?>px;" />
<?php } ?>
<?php } ?>
<?php if ($sy->option['show_search_in_header']) { ?>
<div style="float: right; margin: 1em;">
<form method="get" action="<?php bloginfo('url'); ?>/">
<label class="hidden"><?php _e('Search for:'); ?></label>
<div><input type="text" size="12" value="<?php the_search_query(); ?>" name="s" />
</div>
</form>
</div>
<?php } ?>
		<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
		<div class="description"><?php bloginfo('description'); ?></div>
	</div>
<div id="headermenu">
<ul><?php
 $string = wp_list_pages('title_li=&echo=0&depth=1' );
 $pattern = ' current_page_item "';$replacement = '" id="current"';
 echo str_replace($pattern, $replacement, $string); ?>
</ul>
</div><!-- headermenu -->
<div id="headerbar">
</div>
</div>
