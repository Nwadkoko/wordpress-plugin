let header = $('#header');
let buttonTemplate = Handlebars.templates.buttonHeader;
let popinEmailFormTemplate = Handlebars.templates.popinEmailForm;
header.append(buttonTemplate);
header.append(popinEmailFormTemplate);

let button = $('#header-button');
let popinEmailForm = $('#email-form');
let popinEmailFormClose = $('#email-form-close');

button.click(() => {
    popinEmailForm.show();
});

popinEmailFormClose.click(() => {
    popinEmailForm.hide();
});