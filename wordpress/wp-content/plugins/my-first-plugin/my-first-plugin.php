<?php
/*
Plugin Name: My first plugin
Plugin URI: https://mon-siteweb.com/
Description: First plugin, do nothing
Author: Matthieu Beaussart
Version: 1.0
Author URI: http://mon-siteweb.com/
*/

if(!defined('ABSPATH'))
    exit;

define('MY_FIRST_PLUGIN_VERSION', '1.0.0');
define('MY_FIRST_PLUGIN_FILE', __FILE__);
define('MY_FIRST_PLUGIN_DIR', dirname(MY_FIRST_PLUGIN_FILE));
define('MY_FIRST_PLUGIN_BASENAME', pathinfo((MY_FIRST_PLUGIN_FILE))['filename']);
define('MY_FIRST_PLUGIN_PLUGIN_NAME', MY_FIRST_PLUGIN_BASENAME);
define('MY_FIRST_PLUGIN_PER_PAGE', 30);

foreach(glob(MY_FIRST_PLUGIN_DIR . '/classes/*/*.php') as $filename)
    if(!preg_match('/export/i', $filename))
        if(!@require_once $filename)
            throw new Exception(sprintf(__('Failed to include %s'), $filename));

register_activation_hook( __FILE__, 'myFirstPluginSetup' );

if(is_admin())
    new My_First_Plugin_Admin();
else
    new My_First_Plugin_Front();

function myFirstPluginSetup() {
    $My_First_Plugin_Install = new My_First_Plugin_Install();
    $My_First_Plugin_Install->setup();
}