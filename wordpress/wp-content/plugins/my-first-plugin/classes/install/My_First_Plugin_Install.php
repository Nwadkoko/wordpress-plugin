<?php

class My_First_Plugin_Install {
    function setup() {
        if($this->isTableBaseAlreadyCreated()){
            return;
        }
        global $wpdb;

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $sql_alumni = '
            CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . MY_FIRST_PLUGIN_BASENAME . '_users` (
            `id` INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(100),
            `email` VARCHAR(100),
            `age` INT,
            `phone` VARCHAR(20),
            `team` VARCHAR(255),
            `text` VARCHAR(255),
            `data` DATETIME NULL ) ENGINE=InnoDB
            ';
        return dbDelta($sql_alumni);
    }

    function isTableBaseAlreadyCreated() {
        global $wpdb;

        $sql = 'SHOW TABLES LIKE `' . $wpdb->prefix . MY_FIRST_PLUGIN_BASENAME . '_users`';
        
        return $wpdb->get_var($sql);
    } 
}