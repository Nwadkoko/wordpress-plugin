<?php

add_action(wp_ajax_save_user, array('My_First_Plugin_Action', 'save'));
add_action(wp_ajax_nopriv_save_user, array('My_First_Plugin_Action', 'save'));

class My_First_Plugin_Action {
    public function save() {

        date_default_timezone_set('Europe/Paris');

        $phone = $_POST['phone'];
        $username = $_POST['username'];
        $mail = $_POST['mail'];
        $age = $_POST['age'];
        $team = $_POST['team'];
        $text = $_POST['textUser'];

        global $wpdb;
        $table = $wpdb->prefix . MY_FIRST_PLUGIN_BASENAME . '_users';
        
        $test = $wpdb->insert( 
            $table,
            array( 
                'name' => $username,
                'email' => $mail,
                'age' => $age,
                'team' => $team,
                'text' => $text,
                'phone' => $phone, 
                'data' => date('Y-m-d H-i-s')
            )
        );var_dump($test);
    }
}