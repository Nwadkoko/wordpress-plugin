<?php

class My_First_Plugin_Front
{

    public function __construct()
    {
        $this->setup();
    }

    function setup()
    {
        add_action('wp_enqueue_scripts', array($this, 'handlebarsScript'));
    }

    function handlebarsScript()
    {
        wp_register_script('handle-script', plugins_url(MY_FIRST_PLUGIN_PLUGIN_NAME . "/assets/js/handlebars.js"));
        wp_register_style('style', plugins_url(MY_FIRST_PLUGIN_PLUGIN_NAME . "/assets/style/style.css"));
        wp_register_script( 'custom-jquery', plugins_url(MY_FIRST_PLUGIN_PLUGIN_NAME . '/assets/js/custom-jquery.js'), null, null, true);
        wp_register_script( 'button-template', plugins_url(MY_FIRST_PLUGIN_PLUGIN_NAME . '/assets/templates/compiled-templates/button-header.precompiled.js'), null, null, true);
        wp_register_script( 'popin-email-form-template', plugins_url(MY_FIRST_PLUGIN_PLUGIN_NAME . '/assets/templates/compiled-templates/popinEmailForm.precompiled.js'), null, null, true);
        wp_register_script( 'handlebars-script', plugins_url(MY_FIRST_PLUGIN_PLUGIN_NAME . '/assets/js/handlebars-script.js'), null, null, true);
        wp_enqueue_script('handle-script');
        wp_enqueue_script('template-script');
        wp_enqueue_style('style');
        wp_enqueue_script( 'custom-jquery');
        wp_enqueue_script( 'button-template');        
        wp_enqueue_script( 'popin-email-form-template');       
        wp_enqueue_script( 'handlebars-script');
        wp_enqueue_script('myfirstpluginjs', plugins_url(MY_FIRST_PLUGIN_PLUGIN_NAME . '/assets/js/myfirstpluginjs.js'), array('jquery'), '1.0', true);
        wp_localize_script( 'myfirstpluginjs', 'myfirstpluginjs', array(
            'action' => 'save_user',
            'ajaxurl' => admin_url('admin-ajax.php')
        ));
    }
}
